var http = require('http');
var fs = require('fs');
var pg = require("pg");
var io = require('socket.io');

var port = 5433;
var host = '127.0.0.1';

var conString = "pg://postgres:ordipass@localhost:5432/test";
var customer = new pg.Client(conString);
customer.connect();

var server = http.createServer(function(req, res) {
    fs.readFile('./index.html', 'utf-8', function(error, content) {
        res.writeHead(200, {"Content-Type": "text/html"});
        res.end(content);
    });
});

customer.query("SELECT COUNT(*) FROM tasks WHERE done=false", function (err, result) {
	global.countTask = result.rows[0].count;
});

io = io.listen(server);

io.sockets.on('connection', function (socket) {
    socket.emit('count', global.countTask);
    
    //Send tasks on start to the new customer
	customer.query("SELECT id_task, description, done FROM tasks", function (err, rows) {
		if(rows != undefined){
			var list_tasks = rows.rows;
			for(i=0; i<list_tasks.length; i++){
				socket.emit('serverSendTask', list_tasks[i]);
			}
		}
	});
	
	//When a customer sends a new task
    socket.on('sendNewTask', function (task) {
		customer.query("INSERT INTO tasks (description) VALUES ('"+ task +"')");
		customer.query("SELECT id_task, description, done FROM tasks WHERE description='"+ task +"'", function (err, rows) {
			var addedTask = rows.rows[0];
			global.countTask ++;
			io.sockets.emit('serverSendTask', addedTask);
			io.sockets.emit('count', global.countTask);
		});
    });
    
    //When a customer deletes a task
	socket.on('deleteTask', function (id) {
		customer.query("SELECT done FROM tasks WHERE id_task="+id, function (err, rows) {
			var done = rows.rows[0].done;
			if(done==false){
				global.countTask --;
			}
			io.sockets.emit('count', global.countTask);
		});
		customer.query("DELETE FROM tasks WHERE id_task="+id);
		io.sockets.emit('deletedTask', id);
    });
	
	//When a customer changes the state of a task
	socket.on('doneTask', function (data){
		if(data.value){
			customer.query("UPDATE tasks SET done=TRUE WHERE id_task="+ data.id);
			global.countTask --;
		}else{
			customer.query("UPDATE tasks SET done=FALSE WHERE id_task="+ data.id);
			global.countTask ++;
		}
		io.sockets.emit('count', global.countTask);
		io.sockets.emit('updateTask', {id: data.id, value: data.value});
	});
});

server.listen(8070);
