# README #

## What is this repository for? ##

This repository contains a live tasks project. It's a web application which is used to create tasks collaboratively.

## Requirements ##

The project requires to have node.js and a postgreSQL database on the host server. There must have a postgreSQL user whose name is 'postgres'. I used for this user the password 'ordipass'. If you use a different password, change the database connexion in server.js file, line 10.

## Database configuration ##

The database's name must be 'test' (if you want to modify it, modify the database connexion in server.js file, line 10). The table's name I use is 'tasks' and it has 3 fiels :
- id_task : int autoincrement primary key
- description : varchar (100)
- done : boolean.

## How to run tests ##

To run tests, go to server.js directory and then launch this command :
node server.js

Then on your browser, type this url :
http://localhost:8070
